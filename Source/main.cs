// #define DB_PXLS

using System;
using System.IO;
using System.Drawing;

class App
{
	#region MANUAL

	static void PrintHowToUseMessage()
	{
		const string kBuffer = "======";
		Console.Write(kBuffer);
		Console.Write(" Image to ASCII Art "); // The Title
		Console.WriteLine(kBuffer);

		Console.WriteLine();

		Console.WriteLine("/in:<file>                     The input image file");
		Console.WriteLine("/out:<file>                    The file to put the ascii art in. If this is not provided, the art is printed to the console");
		Console.WriteLine("/scale-down:<decimal number>   The amount to scale the image down by");
		Console.WriteLine("/ccolour                        Turns on colours in the console. Does not work when outputing to a file");
	}

	#endregion

	struct Parameters
	{
		public string ImageLocation;
		public string AsciiOut;
		public float ScaleDown;
		public bool UseColour;
	}

	static bool ParseArgs(string[] args, out Parameters paras)
	{
		paras = new Parameters{AsciiOut = "", ScaleDown = 1};

		foreach (var arg in args)
		{
			if (arg[0] == '/')
			{
				var argElems = arg.Split(':');

				switch (argElems[0].ToLower())
				{
					case "/in":
						paras.ImageLocation = argElems[1];
						break;

					case "/out":
						paras.AsciiOut = argElems[1];
						break;
					
					case "/scale-down":
						paras.ScaleDown = float.Parse(argElems[1]);
						break;
					
					case "/ccolour":
						paras.UseColour = true;
						break;

					default:
						Console.WriteLine("Unrecognised compiler option: " + argElems[0]);
						Console.WriteLine();
						return false;
				}
			}
			else
			{
				if (paras.ImageLocation == string.Empty)
					paras.ImageLocation = arg;
				else if (paras.AsciiOut == string.Empty)
					paras.AsciiOut = arg;
				else
					return false;
			}
		}

		return paras.ImageLocation != string.Empty;
	}

	static double GetBrightness(Color c)
	{
		// Gets the brightness on a scale of 0 to 255
		return Math.Sqrt(
			c.R * c.R * .241 +
			c.G * c.G * .691 +
			c.B * c.B * .068
		);
	}

	const string KPixels = " .-+*wGHM#&%";

	public static void Main(string[] args)
	{
		Parameters paras;

		if (!ParseArgs(args, out paras))
		{
			PrintHowToUseMessage();
			return;
		}

		try
		{
			var image = new Bitmap(paras.ImageLocation);

			if (paras.ScaleDown > image.Height || paras.ScaleDown > image.Width)
			{
				Console.WriteLine("Too much scaling would be applied at scale-down");
				Console.WriteLine("Scaling is set to 1");
				paras.ScaleDown = 1f;
			}

			int newWidth = (int)Math.Ceiling(image.Width / paras.ScaleDown);
			int newHeight = (int)Math.Ceiling(image.Height / paras.ScaleDown);

			Color[] pixels = new Color[newWidth * newHeight];

			int xStep = (int)Math.Round(paras.ScaleDown), yStep = (int)Math.Round(paras.ScaleDown);
#if DB_PXLS
			Console.WriteLine(pixels.Length);
			Console.WriteLine("Old: (" + image.Width + ", " + image.Height + "); New: (" + newWidth + ", " + newHeight + ")");
#endif
			for (var x = 0; x < image.Width; x++)
			{
				for (var y = 0; y < image.Height; y++)
				{
					var col = image.GetPixel(x, y);
					// int i = (int)Math.Round(((double)y / yStep) * xStep + (x / xStep) * yStep);
					var i = (int)Math.Floor((double)y / yStep) * newWidth + (int)Math.Floor((double) x / xStep);

#if DB_PXLS
					Console.WriteLine(x + ", " + y + ": " +	 i);
#endif
					var r = LerpColour(pixels[i].R, col.R);
					var g = LerpColour(pixels[i].G, col.G);
					var b = LerpColour(pixels[i].B, col.B);
					pixels[i] = Color.FromArgb(r, g, b);
				}
			}

#if !DB_PXLS
			using (var writer = (paras.AsciiOut.Length > 1 ? new StreamWriter(paras.AsciiOut) : new StreamWriter(Console.OpenStandardOutput())))
			{
				for (int i = 0; i < pixels.Length; i++)
				{
					if (i % newWidth == 0)
						writer.WriteLine();
					var brightness = GetBrightness(pixels[i]);
					var index = brightness / 255.0 * (KPixels.Length - 1);
					var pixel = KPixels[(int)Math.Round(index)];

					// Refuses to work and can't figure out why
					// if (paras.UseColour)
					// 	Console.ForegroundColor = ClosestConsoleColour(pixels[i]);
					writer.Write(pixel);
				}
			}
#endif
		}
		catch (FileNotFoundException e)
		{
			Console.WriteLine("There is no file " + e.FileName + " to be found");
		}
		catch (Exception e)
		{
			Console.WriteLine("Issue with the output file " + paras.AsciiOut + " : " + e.Message);
		}
	}

	static byte LerpColour(byte a, byte b, float pct = 0.5f)
	{
		return (byte)(a + (b - a) * pct);
	}

	static ConsoleColor ClosestConsoleColour(Color colour)
	{
		// https://stackoverflow.com/questions/1988833/converting-color-to-consolecolor
		ConsoleColor closest = 0;
		double r = colour.R, g = colour.G, b = colour.B, delta = double.MaxValue;

		foreach (ConsoleColor cc in Enum.GetValues(typeof(ConsoleColor)))
		{
			var name = Enum.GetName(typeof(ConsoleColor), cc);
			// Console.WriteLine(name);
			var col = Color.FromName(name);
			var t = Math.Pow(col.R - r, 2.0) + Math.Pow(col.G - g, 2.0) + Math.Pow(col.B - b, 2.0);
			if (t == 0.0)
				return cc;
			
			if (t < delta)
			{
				closest = cc;
				delta = t;
			}
		}

		return closest;
	}
}
