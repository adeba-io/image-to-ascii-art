# C# Image to ASCii Art

Build scripts use `csc.exe` version 4.0.30319 to compile the project. If you're on Windows 10, you can add
`C:\Windows\Microsoft.NET\Framework\v<version no. here>` to the path to get the scripts to work.

## Credits

The initial part of the project, being everything as of commit <>, was completed with the help of [this tutorial](https://www.youtube.com/watch?v=2FGYR6H1Tvc) by [Beer and Code](https://www.youtube.com/user/BeerAndCode).
